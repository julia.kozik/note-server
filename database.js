const sqlite3 = require('sqlite3').verbose()
const md5 = require('md5')

const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
      // Cannot open database
      console.error(err.message)
      throw err
    }
});

console.log('Connected to the SQLite database.')

// Create user table if not exist
db.run(`CREATE TABLE user (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name text, 
    email text UNIQUE, 
    password text, 
    CONSTRAINT email_unique UNIQUE (email)
    )`,
(err) => {
    if (err) {
        // Table already created
    }else{
        // Table just created, creating some rows
        var insert = 'INSERT INTO user (name, email, password) VALUES (?,?,?)'
        db.run(insert, ["admin","admin@example.com",md5("admin123456")])
        db.run(insert, ["user","user@example.com",md5("user123456")])
    }
});  

//Create token table if not exist

db.run (`CREATE TABLE token (
    token text PRIMARY KEY
)`, (err)=>{
    // Do nothing
})

// Create table for Notes

db.run (`CREATE TABLE notes (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    subject text,
    url text,
    note text,
    images text,
    createdAt text,
    updatedAt text
)`,(err)=>{
    // Do nothing
})


module.exports = db

// Create express app
const express = require("express");
const md5 = require("md5");
const jwt = require("jose");
const cors = require("cors");

const db = require("./database.js");
const auth = require("./middleware.js");

const app = express();

app.use(cors());

app.use(express.urlencoded({ extended: true }));
app.use(express.json()); //use this insted of bodyParser
app.use(auth);
// Server port
const HTTP_PORT = 8000;
// Access key
const ACCESS_KEY = "Access Secret";
const REFRESH_KEY = "Refresh Secret";
// Start server
app.listen(HTTP_PORT, () => {
  console.log("Server running on port %PORT%".replace("%PORT%", HTTP_PORT));
});
// Root endpoint
app.get("/", (req, res, next) => {
  res.json({ message: "Ok" });
});

// Create new user
app.post("/api/users/new", (req, res, next) => {
  const errors = [];
  if (!req.body.password) {
    errors.push("No password specified");
  }
  if (!req.body.email) {
    errors.push("No email specified");
  }
  if (errors.length) {
    res.status(400).json({ error: true, message: errors.join(",") });
    return;
  }

  const data = {
    email: req.body.email,
    password: md5(req.body.password),
  };

  const sql = "INSERT INTO user (name, email, password) VALUES (?,?,?)";
  const params = [data.name, data.email, data.password];

  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: true, message: err.message });
      return;
    }
    res.json({
      error: false,
      message: "success",
      email: data.email,
    });
  });
});

// List of users
app.get("/api/users", (req, res, next) => {
  var sql = "select * from user";
  var params = [];
  db.all(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: "success",
      data: rows,
    });
  });
});

// Get user by Id
app.get("/api/users/:id", (req, res, next) => {
  var sql = "select * from user where id = ?";
  var params = [req.params.id];
  db.get(sql, params, (err, row) => {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    if (!row) {
      res.status(404).json({ error: "Not found" });
      return;
    }
    res.json({
      message: "success",
      data: row,
    });
  });
});

// Delete an user
app.delete("/api/users/:id", (req, res, next) => {
  db.run(
    "DELETE FROM user WHERE id = ?",
    req.params.id,
    function (err, result) {
      if (err) {
        res.status(400).json({ error: res.message });
        return;
      }
      res.json({ message: "deleted", changes: this.changes });
    }
  );
});

// Create login endpoint
app.post("/api/login", (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  // Verify user credentials
  // check if email is provided
  // check if password is provided
  if (!password || !email) {
    return res.status(400).json({
      error: true,
      message: "Password or Email is not provided",
    });
  }

  // Get user by email if exist, return error if not
  const sql = "select * from user where email = ?";
  const params = [email];

  db.get(sql, params, async (err, user) => {
    // If user not found return error
    if (err || !user) {
      return res
        .status(400)
        .json({ error: true, message: "Email or password is incorrect." });
    }

    // check is password is valid
    const storedHash = user.password;
    const passwordHash = md5(password);

    if (storedHash !== passwordHash) {
      return res
        .status(400)
        .json({ error: true, message: "Email or password is incorrect." });
    }

    // Generate access and refresh tokens
    const accessToken = await new jwt.UnsecuredJWT({ userId: user.id })
      .setExpirationTime("1h")
      .encode();

    const refreshToken = await new jwt.UnsecuredJWT({ userId: user.id })
      .setExpirationTime("1w")
      .encode();

    // Save refresh token to DB
    const sql = "INSERT INTO token (token) VALUES (?)";
    const params = [refreshToken];
    db.run(sql, params, (err, result) => {
      if (err) {
        return res
          .status(500)
          .json({ error: true, message: "Database error!" });
      }

      // Return tokens to user
      return res.json({
        error: false,
        accessToken: accessToken,
        refreshToken: refreshToken,
      }); // Key:value
    });
  });
});

// Create logout
app.post("/api/logout", (req, res) => {
  // Check if request token is provided
  const refreshToken = req.body.refreshToken;
  if (!refreshToken) {
    return res
      .status(400)
      .json({ error: true, message: "Refresh token is not provided." });
  }

  db.run(
    "DELETE FROM token WHERE token = ?",
    refreshToken,
    function (err, result) {
      if (err) {
        res.status(400).json({ error: true, message: res.message });
        return;
      }
      res.json({ error: false, message: "Logged out" });
    }
  );
});

// Create token endpoint
app.post("/api/refreshtoken", (req, res) => {
  // Check if request token is provided
  const refreshToken = req.body.refreshToken;
  if (!refreshToken) {
    return res
      .status(400)
      .json({ error: true, message: "Refresh token is not provided." });
  }

  // Decoding
  const payload = jwt.UnsecuredJWT.decode(refreshToken); // It contains userId and exp
  const exp = payload.exp;
  const now = Date.now();

  if (exp < now) {
    db.run(
      "DELETE FROM token WHERE token = ?",
      refreshToken,

      (err, result) => {
        // in case of database error
        if (err) {
          console.log("DB error");
        }
      }
    );

    return res.status(400).json({ error: true, message: "Token not valid" });
  }

  db.get(
    "SELECT * FROM token WHERE token = ?",
    refreshToken,
    async (err, result) => {
      if (err) {
        return res
          .status(400)
          .json({ error: true, message: "Token not valid" });
      }
      // Generate access and refresh tokens
      const accessToken = await new jwt.UnsecuredJWT({ userId: payload.userId })
        .setExpirationTime("1h")
        .encode();

      return res.json({ accessToken: accessToken }); //Key:value
    }
  );
});

// Create Note endpoint
app.post("/api/notes", (req, res) => {
  // Get data from request
  const { subject, url, note, images } = req.body;
  const now = new Date().toISOString();
  const createdAt = now;
  const updatedAt = now;

  if (!isNoteValid({ subject, url, note, images })) {
    return res.status(400).json({ error: true, message: "Subject not valid" });
  }

  const sql =
    "INSERT INTO notes (subject, url, note, images, createdAt, updatedAt) VALUES (?,?,?,?,?,?)";
  const params = [
    subject,
    url,
    note,
    (images || []).toString(),
    createdAt,
    updatedAt,
  ];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: true, message: err.message });
      return;
    }
    res.json({
      error: false,
      message: "success",
    });
  });
});

// List of notes
app.get("/api/notes", (req, res, next) => {
  var sql = "select * from notes";
  var params = [];
  db.all(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      error: false,
      message: "success",
      data: rows.map((row) => {
        row.images = row.images ? row.images.split(",") : [];
        return row;
      }),
    });
  });
});

// Delete note
app.delete("/api/notes/:id", (req, res, next) => {
  db.run(
    "DELETE FROM notes WHERE id = ?",
    req.params.id,
    function (err, result) {
      if (err) {
        res.status(400).json({ error: res.message });
        return;
      }

      res.json({
        error: false,
      });
    }
  );
});

// Get note by Id
app.get("/api/notes/:id", (req, res, next) => {
  var sql = "select * from notes where id = ?";
  var params = [req.params.id];
  db.get(sql, params, (err, row) => {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    if (!row) {
      res.status(404).json({ error: "Not found" });
      return;
    }
    res.json({
      message: "success",
      data: row,
    });
  });
});

// Update note
app.put("/api/notes/:id", (req, res, next) => {
  // Step 1 :Get data
  const { subject, url, note, images } = req.body;
  const updatedAt = new Date().toISOString();
  
  // Step 2: Validate data
  if (!isNoteValid({ subject, url, note, images })) {
    return res.status(400).json({ error: true, message: "Subject not valid" });
  }

  // If step 1,2 ok, Update notes
  const sql =
    "UPDATE notes SET subject = ?, url =?, note =?, images = ?, updatedAt = ? WHERE id = ?";
  const params = [
    subject,
    url,
    note,
    (images || []).toString(),
    updatedAt,
    req.params.id,
  ];
  db.run(sql, params, (err, result) => {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      error: false,
      message: "updated",
    });
  });
});

// Note Validator
const isNoteValid = (data) => {
  const { subject, url, note, images } = data;
  if (!subject || typeof subject !== "string") {
    return false;
  }

  if (typeof url !== "string") {
    return false;
  }

  if (typeof note !== "string") {
    return false;
  }

  if (images && Array.isArray(images)) {
    images.forEach((image) => {
      if (typeof image !== "string" || !image.startsWith("http")) {
        return false;
      }
    });
  }

  return true;
};

// Default response for any other request
app.use(function (req, res) {
  res.status(404);
});

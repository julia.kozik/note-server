# Demo server for Note project
Provides CRUD endpoints for user management and notes data.
For the sake of simplicity, this project uses `sqlite` database.

## Get started
Inside the `server` folder run `npm install`

## Run server
`npm start`
Server will be avilable on localhost:8000
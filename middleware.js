const jwt = require("jose");

const openRoutes = ["/api/refreshtoken", "/api/login", "/api/users/new"];

module.exports = function (req, res, next) {
  if (openRoutes.includes(req.url)) {
    return next();
  }

  const token = req.header("token");
  if (!token) return res.status(401).json({ message: "Auth Error" });

  try {
    const payload = jwt.UnsecuredJWT.decode(token); // It contains userId and exp
    const exp = payload.exp;
    const now = Date.now();

    if (exp < now) {
      return res.status(400).json({ error: true, message: "Token not valid" });
    }

    req.userId = payload.userId;
    next();
  } catch (err) {
    return res.status(401).json({ message: "Auth Error" });
  }
};
